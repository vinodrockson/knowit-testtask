package knowit;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import knowit.Application;
import knowit.SimpleDbConfig;
import knowit.controllers.StudentController;
import knowit.repositories.StudentRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = { Application.class,
		SimpleDbConfig.class })
@WebAppConfiguration
//@ActiveProfiles("test")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
public class ApplicationTests {
	@Autowired
	private WebApplicationContext wac;

	@Autowired
	private StudentRepository stuRepo;

	private MockMvc mockMvc;

	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
	}

	@Test
	@DatabaseSetup("dataset.xml")//repository test
	public void countAllStudents() throws Exception {
		assertEquals(7, stuRepo.count());
	}
	
	@Test
	@DatabaseSetup("dataset.xml")//list() test
	public void testGetAllStudents() throws Exception {
		mockMvc.perform(get("/students")).andExpect(
				status().isOk());
	}
	
	@Test
	@DatabaseSetup("dataset.xml")//test for sorting
	public void testFinderMethod() throws Exception {
		assertEquals("Anmol", stuRepo.finderMethod().get(0).getName());
	}
	
	@Test
	//test for upload
	public void testWrongUpload() throws Exception {
		MockMultipartFile firstFile = new MockMultipartFile("file", "hello.xml", "text/xml", "some_xml".getBytes());
  
        mockMvc.perform(MockMvcRequestBuilders.fileUpload("/upload")
                        .file(firstFile)
                        .param("file", "desc"))
                    .andExpect(status().isBadRequest());
	}
	
	@Test//test mean method
	@DatabaseSetup("dataset.xml")
	public void testGetMean() throws Exception {
		StudentController sc = new StudentController();
		//These are grades and average of id=2 in dataset.xml. 
		Float[] test = {40.9f,69.0f,84.0f,76.0f,39.5f};
		assertEquals("61.88", sc.getMean(test).toString());
	}
	
	@Test//test Weighted average method
	@DatabaseSetup("dataset.xml")
	public void testGetWeightAvg() throws Exception {
		StudentController sc = new StudentController();
		//These are grades and weighted average of id=2 in dataset.xml. 
		Float[] test1 = {40.9f,69.0f,84.0f,76.0f,39.5f};
		Float[] test2 = {6f,2f,3f,3f,5f};		
		assertEquals("55.84", sc.getWeightAvg(test1, test2).toString());
	}
}
