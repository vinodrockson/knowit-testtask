package knowit.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

@Entity
@Data
@XmlRootElement
public class Student {

	@Id
	@GeneratedValue
	Long id;
	
	String name;
	Float course1;
	Float course2;
	Float course3;
	Float course4;
	Float course5;	
	Double avggrade;
	Double weightgrade;
}
