package knowit.repositories;

import java.util.List;

import knowit.models.Student;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {

	List<Student> findByNameLike(String name);

	@Query("select s from Student s ORDER BY weightgrade DESC, name ASC")
	List<Student> finderMethod();
}
