package knowit.controllers;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import knowit.models.Student;
import knowit.repositories.StudentRepository;

import org.apache.commons.math.stat.descriptive.DescriptiveStatistics;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

@Controller
public class StudentController {
	
	@Autowired
	StudentRepository srepo;
	
    // Get the sorted student list
	@RequestMapping("/students")
	public String list(Model model){
		model.addAttribute("students", srepo.finderMethod());
		return "students/list";
	}
	
	//Delete a record in student list
	@RequestMapping(value="/students/{id}", method=RequestMethod.DELETE)
	public String delete(Model model, @PathVariable Long id){
		Student student = srepo.findOne(id);
		srepo.delete(student);
		return "redirect:/students";
	}
	
    //upload xml file and store attributes to database
    @RequestMapping(value="/upload", method=RequestMethod.POST)
    public String create(@RequestParam("file") MultipartFile file, @RequestParam("desc") String desc ){
    	String fileName = null;
    	if (!file.isEmpty()) {
            try {
            	// Save XML File
                fileName = file.getOriginalFilename();
                byte[] bytes = file.getBytes();
                BufferedOutputStream buffStream = 
                        new BufferedOutputStream(new FileOutputStream(new File("files/grades.xml")));
                buffStream.write(bytes);
                buffStream.close();
                //XML Parser
                try {

		    		String workingDir = System.getProperty("user.dir");
		    		workingDir = workingDir.replace("\\" , "//");
		    		File newfile = new File(workingDir+"/files/grades.xml");
		    		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		    		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		    		Document doc = dBuilder.parse(newfile);
		    		doc.getDocumentElement().normalize();
		    		NodeList nList = doc.getElementsByTagName("student");

		    		for (int temp = 0; temp < nList.getLength(); temp++) {  
		    			Node nNode = nList.item(temp);
		            	Student stud = new Student();
			    		String[] ctemp = new String[10];
			    		Float[] grades = new Float[5];
			    		Float[] weights = new Float[5];

		    			if (nNode.getNodeType() == Node.ELEMENT_NODE){	
		    				Element eElement = (Element) nNode;	 
		    				stud.setName(eElement.getElementsByTagName("name").item(0).getTextContent());
		    				ctemp=eElement.getElementsByTagName("course1").item(0).getTextContent().split(";");
		    				grades[0]=Float.parseFloat(ctemp[0]);weights[0]= Float.parseFloat(ctemp[1]);  					    				
		    				stud.setCourse1(grades[0]);
		    				ctemp=eElement.getElementsByTagName("course2").item(0).getTextContent().split(";");
		    				grades[1]=Float.parseFloat(ctemp[0]); weights[1]= Float.parseFloat(ctemp[1]);
		    				stud.setCourse2(grades[1]);
		    				ctemp=eElement.getElementsByTagName("course3").item(0).getTextContent().split(";");
		    				grades[2]=Float.parseFloat(ctemp[0]); weights[2]= Float.parseFloat(ctemp[1]); 
		    				stud.setCourse3(grades[2]);
		    				ctemp=eElement.getElementsByTagName("course4").item(0).getTextContent().split(";");
		    				grades[3]=Float.parseFloat(ctemp[0]); weights[3]= Float.parseFloat(ctemp[1]);
		    				stud.setCourse4(grades[3]);
		    				ctemp=eElement.getElementsByTagName("course5").item(0).getTextContent().split(";");
		    				grades[4]=Float.parseFloat(ctemp[0]); weights[4]= Float.parseFloat(ctemp[1]);
		    				stud.setCourse5(grades[4]);
		    				stud.setAvggrade(getMean(grades));
		    				stud.setWeightgrade(getWeightAvg(grades, weights));
		    				srepo.saveAndFlush(stud);
	    				}
                } 
                }catch (Exception e) {
                	e.printStackTrace();
               }
              return "redirect:/students";
            } catch (Exception e) {
                return "You failed to upload " + fileName + ": " + e.getMessage();
            }
        } else {
            return "Unable to upload. File is empty.";
        }
    }
    
    //calculate mean
    public Double getMean (Float[] grades) {
	  DescriptiveStatistics stats=new DescriptiveStatistics();
	  for (int i=0; i < grades.length; i++) {
	    stats.addValue(grades[i]);
	  }
	  
	  Double mean = stats.getMean();
	  return Math.round(mean*100.0)/100.0;	   
    }
    
    //calculate weighted average    
    public Double getWeightAvg (Float [] data, Float [] weights) {
    	int i, n = data.length;    			
    	double sum = 0;
    	double weight = 0;    	
    	for (i=0; i<n; i++) {
            double d = data [i], w = weights [i];
            if (d != 0 && w != 0) {
                sum = sum+(d * w);
                weight = weight+w;
            }
    	}
    	
    	if (sum == 0 || weight == 0 || weight == 0.)
            return 0.;
    	
    	Double weightavg = sum/weight;    	
    	return Math.round(weightavg*100.0)/100.0;	
    }
}
